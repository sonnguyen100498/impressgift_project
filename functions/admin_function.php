<?php
define("BASE_URL", "http://localhost:8080/hoc_php/Impressgift_project");

function getRoleName($id) {
    if ($id == 1){
        return "ADMIN";
    } else if ($id == 2) {
        return "MANAGER";
    }
    return null;
}

function getAdminUrl($controller, $action, $param = null) {
    if (!empty($param)) {
        return BASE_URL."/admin?controller=$controller&action=$action&param=$param";
    }
    return BASE_URL."/admin?controller=$controller&action=$action";
}

function getAvatar($url) {
    return '<img src="'.$url.'" width="100">';
}

function redirectURL ($url) {
    echo '<script>window.location.href = "'.$url.'"</script>';
    exit();
}

function getFeatureProduct($value) {
    if ($value == 1) {
        return "Feature Product";
    } else if ($value == 0) {
        return "Not Feature Product";
    }
    return null;
}

function getUrl($action, $param = null){
    if (!empty($param)) {
        return BASE_URL."?action=$action&param=$param";
    }
    return BASE_URL."?action=$action";
}

function redirect_Url($action) {
    return BASE_URL."?action=$action";
}



?>