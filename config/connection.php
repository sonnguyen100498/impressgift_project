<?php
/**
 * connect to database
 * @return mysqli
 */

function connect_db() {
    $connect = mysqli_connect('localhost', 'root', '', 'impressgift_db_new');
    if (mysqli_connect_errno()) {
        echo "connect to db failed!";
        die();
    }
    return $connect;
}

/**
 * close connect to db
 * @param mysqli $connect mysqli object
 * @return void
 */

function close_db_connect($connect) {
    mysqli_close($connect);
}