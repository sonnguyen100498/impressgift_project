<?php

$connect = connect_db();
$productID = (!empty($_GET['param']))?$_GET['param']:null;
if (!$productID) {
    redirectURL(BASE_URL);
}

$productQuery = mysqli_query($connect, "select * from products where id = $productID");
if ($productQuery->num_rows==0) {
    redirectURL(BASE_URL);
}
$product = mysqli_fetch_array($productQuery);
$productImageQuery = mysqli_query($connect, "select * from product_images WHERE product_id=$productID");
$productImages=[];
if ($productImageQuery->num_rows>0) {
    while ($productImage = mysqli_fetch_array($productImageQuery)) {
        $productImages[]=$productImage;
    }
}

?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Home</title>
</head>
<body>
<div class="container">
<div class="content">
    <div class="row">
        <div class="col-3">
            <p class="mt-4">
                <a href="<?php echo redirect_Url("home")?>" class="text-decoration-none">
                    <i class="fa fa-home" aria-hidden="true"></i>
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    <a href="<?php echo redirect_Url("Our_Product");?>" class="text-decoration-none lk">Our Products</a>
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    <a href="product_detail.php" class="text-decoration-none lk">Lorem ipsum dolor</a>
                </a>
            </p>
            <?php include "menu_left.php" ?>
        </div>
        <div class="col-9">
            <div class="row">
                <div class="col-4">
                    <?php
                        if (!empty($productImages)) {?>
                            <div class="product_detail">
                                <img class="image" src="<?php echo $productImages[0]['image']?>" alt="error image">
                            </div>
                        <?php
                        }
                    ?>
                </div>
                <div class="col-5 mt-5 pt-5">
                    <p><strong><?php echo $product['name']?></strong></p>
                    <p class="product_info"><?php echo $product['short_description']?> </p>
                    <p class="price">$<?php echo $product['price']?></p>
                    <a href="#" class="btn-enquiry text-decoration-none">Enquiry Now</a>
                    <br><br>
                    <a href="#" class="social_media_face"><i class="fa fa-facebook facboek" aria-hidden="true"> facebook</i></a>
                    <a href="#" class="social_media_twitter"><i class="fa fa-twitter" aria-hidden="true">Twitter</i></a>
                    <a href="#" class="social_media_google"><i class="fa fa-google-plus google" aria-hidden="true">Google+</i></a>
                    <a href="#" class="social_media_pinterest"><i class="fa fa-pinterest-p" aria-hidden="true">Pinterest</i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <p>Description</p><br>
            <p><?php echo $product['description']?> </p>
            <div class="related_product text-center">
                <p class="text-uppercase text text-center font-weight-bold mt-5 pt-5">Related Products</p>
                <div class="product float-start">
                    <div class="image">
                        <a href="#"><img class=" pt-4 pl-3" src="http://localhost:8080/hoc_php/hoc_tren_lop/website/Impressgift/public/imgs/product1.png" alt=""></a>
                    </div>
                    <br>
                    <p class="m-0"><a href="#" class="text-decoration-none link pl-4">iMp4102-Memo Desk Set</a></p>
                    <br>
                    <i class="home-price fa fa-usd">10</i>
                    <br>
                    <a href="#" class="btn-add text-decoration-none"> Add to enquery</a>
                </div>
                <div class="product float-start">
                    <div class="image">
                        <a href="#"><img class=" pt-4 pl-3" src="http://localhost:8080/hoc_php/hoc_tren_lop/website/Impressgift/public/imgs/product2.png" alt=""></a>
                    </div>
                    <br>
                    <p class="m-0"><a href="#" class="text-decoration-none link pl-4">iMp4101-MemoPad</a></p>
                    <br>
                    <i class="home-price fa fa-usd">15</i>
                    <br>
                    <a href="#" class="btn-add text-decoration-none"> Add to enquery</a>
                </div>
                <div class="product float-start">
                    <div class="image">
                        <a href="#"><img class=" pt-4 pl-3" src="http://localhost:8080/hoc_php/hoc_tren_lop/website/Impressgift/public/imgs/product3.png" alt=""></a>
                    </div>
                    <br>
                    <p class="m-0"><a href="#" class="text-decoration-none link pl-4">iDW4701ST-AntiSlip Suction</a></p>
                    <br>
                    <i class="home-price fa fa-usd">12</i>
                    <br>
                    <a href="#" class="btn-add text-decoration-none"> Add to enquery</a>
                </div>
                <div class="product float-start">
                    <div class="image">
                        <a href="#"><img class=" pt-4 pl-3" src="http://localhost:8080/hoc_php/hoc_tren_lop/website/Impressgift/public/imgs/product4.png" alt=""></a>
                    </div>
                    <br>
                    <p class="m-0"><a href="#" class="text-decoration-none link pl-4">iDW5001B-Rectangle MEMO</a></p>
                    <br>
                    <i class="home-price fa fa-usd">10</i>
                    <br>
                    <a href="#" class="btn-add text-decoration-none"> Add to enquery</a>
                </div>
                <div class="product float-start">
                    <div class="image">
                        <a href="#"><img class=" pt-4 pl-3" src="http://localhost:8080/hoc_php/hoc_tren_lop/website/Impressgift/public/imgs/product5.png" alt=""></a>
                    </div>
                    <br>
                    <p class="m-0"><a href="#" class="text-decoration-none link pl-4">iMp4102-Memo Desk Set</a></p>
                    <br>
                    <i class="home-price fa fa-usd">10</i>
                    <br>
                    <a href="#" class="btn-add text-decoration-none"> Add to enquery</a>
                </div>
                <div class="product float-start">
                    <div class="image">
                        <a href="#"><img class=" pt-4 pl-3" src="http://localhost:8080/hoc_php/hoc_tren_lop/website/Impressgift/public/imgs/product6.png" alt=""></a>
                    </div>
                    <br>
                    <p class="m-0"><a href="#" class="text-decoration-none link pl-4">iMp4101-MemoPad</a></p>
                    <br>
                    <i class="home-price fa fa-usd">10</i>
                    <br>
                    <a href="#" class="btn-add text-decoration-none"> Add to enquery</a>
                </div>
                <div class="product float-start">
                    <div class="image">
                        <a href="#"><img class=" pt-4 pl-3" src="http://localhost:8080/hoc_php/hoc_tren_lop/website/Impressgift/public/imgs/product7.png" alt=""></a>
                    </div>
                    <br>
                    <p class="m-0"><a href="#" class="text-decoration-none link pl-4">iDW4701ST-AntiSlip Suction</a></p>
                    <br>
                    <i class="home-price fa fa-usd">20</i>
                    <br>
                    <a href="#" class="btn-add text-decoration-none"> Add to enquery</a>
                </div>
                <div class="product float-start">
                    <div class="image">
                        <a href="#"><img class=" pt-4 pl-3" src="http://localhost:8080/hoc_php/hoc_tren_lop/website/Impressgift/public/imgs/product8.png" alt=""></a>
                    </div>
                    <br>
                    <p class="m-0"><a href="#" class="text-decoration-none link pl-4">iDW5001B-Rectangle MEMO</a></p>
                    <br>
                    <i class="home-price fa fa-usd">10</i>
                    <br>
                    <a href="#" class="btn-add text-decoration-none"> Add to enquery</a>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
</div>
</body>