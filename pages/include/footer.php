<?php
$connect = connect_db();
$sql = "SELECT * FROM contacts WHERE 1";
$contactsQuery = mysqli_query($connect, $sql );
if($contactsQuery->num_rows>0){
    $contact=mysqli_fetch_array($contactsQuery);
} else {
    $contact = null;
}



?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>HOME</title>
    <link rel="stylesheet" href="http://localhost:8080/hoc_php/Impressgift_new/public/css/css-frontend/style.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,
    300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="http://localhost:8080/hoc_php/Impressgift_new/public/libs/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="../../public/libs/bootstrap_new/css/bootstrap.min.css">

</head>
<body>
<div class="footer">
    <div class="footer-top">
        <div class="row">
            <div class="col-4">
                <img class="img-logo" src="http://localhost:8080/hoc_php/Impressgift_new/public/imgs/footter_logo.png">
            </div>
            <div class="col-8">
                <div class="footer-content footer-impressgift float-start ">
                    <h5 class="text-uppercase">Impressgift</h5>
                    <hr width="20%" align="left">
                    <p><i class="ic-footer fa fa-map-marker"></i> <span><?php echo $contact['name']?></span></p>
                    <p><i class="ic-footer fa fa-phone"></i> <span><?php echo $contact['phone_number']?></span></p>
                    <p><i class="ic-footer fa fa-envelope-o"></i> <span><?php echo $contact['email']?></span></p>
                    <p><i class="ic-footer fa fa-clock-o"></i> <span><?php echo $contact['content_text']?></span></p>
                </div>
                <div class="footer-content footer-quick-link float-start">
                    <h5 class="text-uppercase">Quick link</h5>
                    <hr width="40%" align="left">
                    <ul style="list-style: none" class="p-0 m-0">
                        <li>Home</li>
                        <li>About us</li>
                        <li>Project</li>
                        <li>Our products</li>
                        <li>Contact us</li>
                    </ul>
                </div>
                <div class="footer-content footer-signup float-start">
                    <h5 class="text-uppercase">Signup for our newsletter</h5>
                    <hr width="15%" align="left">
                    <p>Receive our lastest updates about our products and promotions</p>
                    <form>
                        <input placeholder="Enter your email address "><button class="btn-subscribe">SUBSCRIBE</button>
                    </form>
                    <h6 class="text-uppercase stay-connected">Stay Connected</h6>
                    <a class="fa fa-facebook facboek float-start"></a>
                    <a class="fa fa-google-plus google float-start"></a>
                    <a class="fa fa-instagram instagram float-start"></a>
                    <a class="fa fa-youtube-play youtube float-start"></a>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <p class="pt-4">Copyright 2018 <a class="text-decoration-none" href="#"><b>Impressgift</b></a>.All Right Reserved.</p>

    </div>
</div>

</body>