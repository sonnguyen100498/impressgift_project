<?php
$connect = connect_db();

$sql = "SELECT p.id,p.name,p.price,product_images.image,product_images.id as image_id FROM products AS p LEFT JOIN product_images ON product_images.id=(SELECT product_images.id from product_images
 WHERE p.id=product_images.product_id ORDER BY product_images.id ASC LIMIT 1) ORDER BY p.id DESC LIMIT 24";
$productsQuery = mysqli_query($connect, $sql);
$products = [];
if ($productsQuery->num_rows > 0) {
    while ($product = mysqli_fetch_array($productsQuery)) {
        $products[] = $product;
    }
}
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Product</title>
</head>
<body>
<div class="container">
    <div class="row product-row">
        <?php include "menu_left.php" ?>
        <div class="col-9">
            <h3 class="text-uppercase">Our Products</h3>

            <?php
            foreach ($products as $product) { ?>
                <div class="product float-start mb-5">
                    <div class="image">
                        <img src="<?php echo $product['image'] ?>">
                    </div>
                    <br>
                    <a href="#" class="text-decoration-none home-product-name"><?php echo $product['name'] ?></a>
                    <br>
                    <i class="home-price fa fa-usd"><?php echo $product['price'] ?></i>
                    <br>
                    <a href="#" class="btn-add text-decoration-none"> Add to enquery</a>
                </div>
            <?php } ?>
            <div class="clearfix"></div>
            <a href="#" class="text-uppercase btn-read-more text-decoration-none">Read more</a>
        </div>
    </div>

</div>
</body>
