<?php
$connect = connect_db();
$sql = "SELECT * FROM contacts WHERE 1";
$contactQuery = mysqli_query($connect, $sql);
if ($contactQuery ->num_rows > 0) {
    $contact = mysqli_fetch_array($contactQuery);
}

?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Contact Us</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-5">
            <h3 class="mb-4 mt-5">Get in touch with us</h3>
            <ul class="p-0 contact-info">
                <li>
                    <i class="fa fa-map-marker"><span><?php echo $contact['name'] ?></span></i>
                </li>
                <li>
                    <i class="fa fa-phone"><span><?php echo $contact['phone_number'] ?></span></i>
                </li>
                <li>
                    <i class="fa fa-envelope-o"><span><?php echo $contact['email'] ?></span></i>
                </li>
            </ul>
            <form>
                <div class="info mt-5">
                    <div class="float-start info-item">
                        <span>First name</span>
                        <br>
                        <input class="input-info">
                    </div>
                    <div class="float-start info-item">
                        <span>Last name</span>
                        <br>
                        <input class="input-info">
                    </div>
                    <div class="float-start info-item">
                        <span>email</span>
                        <br>
                        <input class="input-info">
                    </div>
                    <div class="float-start info-item">
                        <span>phone</span>
                        <br>
                        <input class="input-info">
                    </div>
                    <div class="float-start mt-4 info-item ">
                        <span>Request detail</span>
                        <br>
                        <input class="info-request">
                    </div>
                    <div class="clearfix"></div>
                    <button type="submit" class="submit mt-5">SUBMIT REQUEST</button>
                </div>
            </form>

        </div>
        <div class="col-7">
            <img class="mt-5 mb-5" src="http://localhost:8080/hoc_php/Impressgift_new/public/imgs/map.png">
        </div>
    </div>

</div>

</body>

