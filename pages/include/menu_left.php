<?php
$connect = connect_db();
$categoriesQuery = mysqli_query($connect, "SELECT * FROM  categories ORDER BY id ASC ");
$categories = [];
if ($categoriesQuery->num_rows > 0) {
    while ($category = mysqli_fetch_array($categoriesQuery)) {
        $categories[] = $category;
    }
}
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Menu left</title>
    <link rel="stylesheet" href="http://localhost:8080/hoc_php/Impressgift_new/public/css/css-frontend/style.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,
    300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="http://localhost:8080/hoc_php/Impressgift_new/public/libs/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="http://localhost:8080/hoc_php/Impressgift_new/public/libs/bootstrap_new/css/bootstrap.min.css">
</head>
<body>
<div class="p-0 m-0 col-3 home-menu-left">
    <ul class="mt-5">
        <?php
        foreach ($categories as $category) {?>
            <li>
                <a href="#" class="text-decoration-none">
                    <span class="float-start"><?php echo $category['name'] ?> </span>
                    <i class="fa fa-angle-right float-end"></i>
                </a>
            </li>

        <?php } ?>
    </ul>

    <div class="home-content-left mt-5">
        <img src="http://localhost:8080/hoc_php/Impressgift_new/public/imgs/bg.png" class="img1">
        <img src="http://localhost:8080/hoc_php/Impressgift_new/public/imgs/text.png" class="img2">
        <img src="http://localhost:8080/hoc_php/Impressgift_new/public/imgs/img1.png" class="img3">
        <img src="http://localhost:8080/hoc_php/Impressgift_new/public/imgs/img2.png" class="img4">
        <a href="#" class="text-decoration-none">View now<i class="fa fa-angle-right text-uppercase"></i></a>
    </div>
</div>
</body>

