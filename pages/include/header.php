<?php
include "functions/admin_function.php";
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>HOME</title>
    <link rel="stylesheet" href="http://localhost:8080/hoc_php/Impressgift_new/public/css/css-frontend/style.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,
    300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="http://localhost:8080/hoc_php/Impressgift_new/public/libs/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="http://localhost:8080/hoc_php/Impressgift_new/public/libs/bootstrap_new/css/bootstrap.min.css">
<!--    <script type="text/javascript" src="../../public/libs/bootstrap_new/jquery-1.11.3.min.js"></script>-->

</head>
<body>
<div class="header mb-2">
    <div class="container mt-3">
        <div class="row">
            <div class="col-4">
                <img src="http://localhost:8080/hoc_php/Impressgift_new/public/imgs/logo.png">
            </div>
            <div class="col-8 mt-4">
                <div class="header-right">
                    <div class="header-right-item float-start">
                        <img class="float-start pt-1 pr-2" src="http://localhost:8080/hoc_php/Impressgift_new/public/imgs/header_car.png">
                        <div class="header-right-item-text ">
                            <h6 class="m-0 text-uppercase">Free shipping</h6>
                            <p class="m-0">Free shipping on all order</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="header-right-item float-start">
                        <img class="float-start pt-1 pr-2" src="http://localhost:8080/hoc_php/Impressgift_new/public/imgs/header_phone.png">
                        <div class="header-right-item-text ">
                            <h6 class=" m-0 text-uppercase">Hotline</h6>
                            <p class="m-0">+65 6876 0079</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="header-right-item float-start">
                        <img class="float-start pt-1 pr-2" src="http://localhost:8080/hoc_php/Impressgift_new/public/imgs/header_card.png">
                        <div class="header-right-item-text">
                            <h6 class="m-0 text-uppercase">Enquiry card</h6>
                            <p class="m-0">(0)</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="menu">
    <div>
        <div class="menu-bar m-0 p-0 ">
            <div class="container">
                <ul class="float-start ">
                    <li class="text-uppercase active"><a href=<?php echo redirect_Url("home");?>>Home</a></li>
                    <li class="text-uppercase "><a href=<?php echo redirect_Url("about");?>>About us</a></li>
                    <li class="text-uppercase "><a href=<?php echo redirect_Url("project");?>>Project</a></li>
                    <li class="text-uppercase "><a href=<?php echo redirect_Url("product");?>>Our Product</a></li>
                    <li class="text-uppercase "><a href=<?php echo redirect_Url("home");?>>Testimonial</a></li>
                    <li class="text-uppercase "><a href=<?php echo redirect_Url("contact_us");?>>Contact us</a></li>
                </ul>
                <form class="float-start search">
                    <input style="border:none" placeholder="search"><button><i style="color: white" class="fa fa-search" aria-hidden="true"></i></button>
                </form>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

</body>