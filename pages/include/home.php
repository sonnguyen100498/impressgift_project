<?php
$connect = connect_db();
$categoriesQuery = mysqli_query($connect, "SELECT * FROM  categories ORDER BY id ASC ");
$categories = [];
if ($categoriesQuery->num_rows > 0) {
    while ($category = mysqli_fetch_array($categoriesQuery)) {
        $categories[] = $category;
    }
}

// lay ra nhung san pham feature product
$sql="SELECT p.id,p.name,p.price,product_images.image,product_images.id as image_id FROM products AS p LEFT JOIN
 product_images ON product_images.id=(SELECT product_images.id from product_images
 WHERE p.id=product_images.product_id ORDER BY product_images.id ASC LIMIT 1) WHERE p.is_feature=1 ORDER BY p.id DESC LIMIT 8";
$feature_productsQuery=mysqli_query($connect,$sql);
$feature_products=[];
if($feature_productsQuery->num_rows>0){
    while ($feature_product=mysqli_fetch_array($feature_productsQuery)){
        $feature_products[]=$feature_product;
    }
}

// lay ra nhung san pham new product

$sql="SELECT p.id,p.name,p.price,product_images.image,product_images.id as image_id FROM products AS p LEFT JOIN 
product_images ON product_images.id=(SELECT product_images.id from product_images
 WHERE p.id=product_images.product_id ORDER BY product_images.id ASC LIMIT 1) ORDER BY p.id DESC LIMIT 4";
$new_productsQuery=mysqli_query($connect,$sql);
$new_products=[];
if($new_productsQuery->num_rows>0){
    while ($new_product=mysqli_fetch_array($new_productsQuery)){
        $new_products[]=$new_product;
    }
}
// lay ra nhung san pham thuoc category gift sets
$sql= "SELECT id FROM categories WHERE name='Staioneries'";
$connect=connect_db();
$category_idQuery=mysqli_query($connect,$sql);
if($category_idQuery->num_rows>0){
    $category_id=mysqli_fetch_array($category_idQuery);
}

//$sql="SELECT * FROM products WHERE category_id=$category_id[0] LIMIT 4";
$sql="SELECT p.id,p.name,p.price,product_images.image,product_images.id as image_id FROM products AS p LEFT JOIN 
product_images ON product_images.id=(SELECT product_images.id from product_images
 WHERE p.id=product_images.product_id ORDER BY product_images.id ASC LIMIT 1) WHERE p.category_id=$category_id[0] ORDER BY p.id DESC LIMIT 4";
$connect=connect_db();
$productsQuery=mysqli_query($connect,$sql);
$products=[];
if($productsQuery->num_rows>0){
    while ($product=mysqli_fetch_array($productsQuery)){
        $products[]=$product;
    }
}

//echo "<pre>";
//var_dump($categories);
//echo "</pre>";
//die();

?>


<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Home</title>
</head>
<body>
<div class="container">
    <div class="row home-row">
        <?php include "menu_left.php" ?>
        <div class="col-9 mt-4">
            <h3 class="text-uppercase text-center">Feature products</h3>
            <?php foreach ($feature_products as $feature_product) {?>
            <div class="product float-start">
                <div class="image">
                    <a href="<?php echo getUrl("product_detail", $feature_product['id'])?>"><img src="<?php echo $feature_product['image'] ?>"></a>
                </div>
                <br>
                <a href="<?php echo getUrl("product_detail", $feature_product['id'])?>" class="text-decoration-none home-product-name"><?php echo $feature_product['name'] ?></a>
                <br>
                <i class="home-price fa fa-usd"><?php echo $feature_product['price'] ?></i>
                <br>
                <a href="#" class="btn-add text-decoration-none"> Add to enquery</a>
            </div>
            <?php } ?>
            <a href="#" class="text-uppercase text-decoration-none home-view-all">View all
                <i class="fa fa-angle-right"></i></a>
            <div class="img_class1">
                <img class="img9" src="http://localhost:8080/hoc_php/Impressgift_new/public/imgs/product9.png">
                <img class="img10" src="http://localhost:8080/hoc_php/Impressgift_new/public/imgs/product10.png">
                <img class="img11" src="http://localhost:8080/hoc_php/Impressgift_new/public/imgs/product11.png">
            </div>
            <div class="img_class2">
                <img class="img12" src="http://localhost:8080/hoc_php/Impressgift_new/public/imgs/bg1.png">
                <img class="img13" src="http://localhost:8080/hoc_php/Impressgift_new/public/imgs/text1.png">
                <img class="img14" src="http://localhost:8080/hoc_php/Impressgift_new/public/imgs/product12.png">
                <img class="img15" src="http://localhost:8080/hoc_php/Impressgift_new/public/imgs/product13.png">
            </div>

            <h3 class="mt-5 mb-4 text-uppercase text-center">New Products</h3>

            <?php foreach ($new_products as $new_product) { ?>
            <div class="product float-start">
                <div class="image">
                    <a href="<?php echo getUrl("product_detail", $new_product['id'])?>"><img src="<?php echo $new_product['image'] ?>"></a>
                </div>
                <br>
                <a href="<?php echo getUrl("product_detail", $new_product['id'])?>" class="text-decoration-none home-product-name"><?php echo $new_product['name'] ?></a>
                <br>
                <i class="home-price fa fa-usd"><?php echo $new_product['price'] ?></i>
                <br>
                <a href="#" class="btn-add text-decoration-none"> Add to enquery</a>
            </div>
            <?php } ?>

            <a href="#" class="mt-4 mb-4 text-uppercase text-decoration-none home-view-all">View all
                <i class="fa fa-angle-right"></i></a>
            <h3 class="text-uppercase text-center">Gift Sets & Baskets</h3>
            <?php foreach ($products as $product) { ?>
            <div class="product float-start">
                <div class="image">
                    <a href="<?php echo getUrl("product_detail", $product['id'])?>"><img src="<?php echo $product['image'] ?>"></a>
                </div>
                <br>
                <a href="<?php echo getUrl("product_detail", $product['id'])?>" class="text-decoration-none home-product-name"><?php echo $product['name'] ?></a>
                <br>
                <i class="home-price fa fa-usd"><?php echo $product['price'] ?></i>
                <br>
                <a href="#" class="btn-add text-decoration-none"> Add to enquery</a>
            </div>
            <?php }?>

            <a href="#" class="mt-4 mb-4 text-uppercase text-decoration-none home-view-all">View all
                <i class="fa fa-angle-right"></i></a>
        </div>
        <div class="clearfix"></div>
    </div>

</div>
</body>
