
    <!DOCTYPE html>
    <head>
        <meta charset="UTF-8">
        <title>About</title>
    </head>
    <body>
    <div class="container">
        <div class="row">
            <div class="col-6 content-left">
                <a href="home.php"><i class="fa fa-home" style="opacity: 50% ; color: #9A9A9A"></i></a>
                <i class="fa fa-angle-right" style="opacity: 50%;"></i>
                <span style="opacity: 50%;"> About Us</span>
                <p class="para">Impress Gift specialise in corporate gift in Singapore. We pride ourselves in giving
                    fast and reliable Quality
                    services to all our customer. We provide the Best Gifting to you. We manage import and export on
                    gifts to
                    Europe and Asia. We have our very own sourcing Department in our factory which gives us the
                    advantage to give
                    you Direct Factory Pricing.</p>
                <h6>Why Impress Gift</h6>
                <div class="about-item">
                    <ul class="mb-5 p-0">
                        <li>
                            <i class="fa fa-check"></i><span>Because we believed that</span>
                        </li>
                        <li>
                            <i class="fa fa-check"></i><span>A GIFT that is handed out, will provide an impression to the customer.</span>
                        </li>
                        <li>
                            <i class="fa fa-check"></i><span>A GIFT that is handed out, represent your company</span>
                        </li>
                        <li>
                            <i class="fa fa-check"></i><span>A GIFT that is handed out, creates a first impression BONDing between</span>
                        </li>
                        <li>
                            <i class="fa fa-check"></i><span>We believe in QUALITY Assurance</span>
                        </li>
                        <li>
                            <i class="fa fa-check"></i><span>We believe in High Perceive Value and Practicality of Gifting</span>
                        </li>
                        <li>
                            <i class="fa fa-check"></i><span>We believe in Advertising and Promotional branding with Gifting</span>
                        </li>
                        <li>
                            <i class="fa fa-check"></i><span>We love the Season OF MPRESSIVE GIFTING</span>
                        </li>
                        <li>
                            <i class="fa fa-check"></i><span>LASTLY, Providing EXCELLENT CUSTOMER SERVICE is our PRACTISE.</span>
                        </li>
                    </ul>
                </div>

            </div>
            <div class="col-6 content-right">
                <img src="http://localhost:8080/hoc_php/Impressgift_new/public/imgs/img-about.png">
            </div>

        </div>

    </div>

    </body>
