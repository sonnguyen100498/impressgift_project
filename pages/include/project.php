<?php

$connect = connect_db();
$sql = "SELECT * FROM projects WHERE 1";
$projectQuery = mysqli_query($connect, $sql);
$projects = [];
if ($projectQuery->num_rows>0) {
    while ($project = mysqli_fetch_array($projectQuery)) {
        $projects[] = $project;
    }
}

?>


<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Project</title>
</head>
<body>
<div class="container">
    <div class="project-content">
        <a href="home.php" class="mt-4"><i class="fa fa-home" style="opacity: 50% ; color: #9A9A9A"></i></a>
        <i class="fa fa-angle-right" style="opacity: 50%;"></i>
        <span style="opacity: 50%;">Project</span>
    </div>
    <div class="row">
        <?php foreach ($projects as $project) {?>
            <div class="col-3">
                <img src=<?php echo $project['image'] ?>>
                <a class="text-decoration-none" href="#">
                    <p class="text-center pt-3">
                        <?php echo $project['name'] ?>
                        <br>
                        Bottle
                    </p>
                </a>
            </div>
        <?php }?>
    </div>
</div>

</body>
