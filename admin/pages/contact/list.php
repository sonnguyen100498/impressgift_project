<?php
?>

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-8">
                <h3 class="card-title mb-0">
                    Contact Management |
                    <small>List contacts</small>
                </h3>
            </div>
            <div class="col-4 text-right">
                <a href="<?php echo getAdminUrl('contact', 'addnew')?>" class="btn-primary btn btn-sm">
                    <i class="fa fa-plus">Add New Information</i></a>
            </div>
        </div>
        <?php
        $connect = connect_db();
        $sql = "select * from contacts ORDER BY id DESC ";
        $contact_query = mysqli_query($connect, $sql);
        close_db_connect($connect);
        ?>
        <div class="mt-4">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <td><strong>ID</strong></td>
                        <td><strong>Name</strong></td>
                        <td><strong>Email</strong></td>
                        <td><strong>Phone Number</strong></td>
                        <td><strong>Content text</strong></td>
                        <td><strong>Created at</strong></td>
                        <td><strong>Action</strong></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($contact_query->num_rows>0){
                        while ($contact=mysqli_fetch_array($contact_query)){
                            ?>
                            <tr>
                                <td><?php echo $contact['id']?></td>
                                <td><?php echo $contact['name']?></td>
                                <td><?php echo $contact['email']?></td>
                                <td><?php echo $contact['phone_number']?></td>
                                <td><?php echo $contact['content_text']?></td>
                                <td><?php echo $contact['created_at']?></td>
                                <td><a href="<?php echo getAdminUrl('contact','edit',$contact['id']) ?>" class="btn btn-warning">Edit</a></td>
                                <td><a href="<?php echo getAdminUrl('contact','delete',$contact['id']) ?>" class="btn btn-danger">Delete</a></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
