<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-8">
                <h3 class="card-title mb-0">
                    Contacts Management |
                    <small>Add new contact</small>
                </h3>
            </div>
        </div>
        <form action="" method="post" enctype="multipart/form-data">
            <div class="form-group mt-4">
                <label for="name">Name</label>
                <input type="text" id="name" name="name" class="form-control" required>
            </div>
            <div class="form-group mt-4">
                <label for="email">Email</label>
                <input type="email" id="email" name="email" class="form-control" required>
            </div>
            <div class="form-group mt-4">
                <label for="phone_number">Phone Number</label>
                <input type="number" id="phone_number" name="phone_number" class="form-control" required>
            </div>
            <div class="form-group mt-4">
                <label for="content_text">Content Text</label>
                <input type="text" id="content_text" name="content_text" class="form-control" required>
            </div>
            <hr>
            <div class="row">
                <div class="col-6">
                    <button type="submit" name="submit" class="btn btn-primary">Add new information</button>
                </div>
                <div class="col-6 text-right">
                    <a class="btn btn-success" href="<?php echo getAdminUrl('contact','list')?>">Cancel</a>
                </div>
            </div>

        </form>
    </div>
</div>

<?php
if (isset($_POST['submit'])) {
    $name = (!empty($_POST['name']))?$_POST['name']:null;
    $email = (!empty($_POST['email']))?$_POST['email']:null;
    $phone_number = (!empty($_POST['phone_number']))?$_POST['phone_number']:null;
    $content_text = (!empty($_POST['content_text']))?$_POST['content_text']:null;
    $connect = connect_db();
    $sql="SELECT * FROM contacts WHERE name='$name' AND email='$email' AND phone_number='$phone_number' AND content_text='$content_text'";
    $contact=mysqli_query($connect,$sql);
    close_db_connect($connect);
    if($contact->num_rows>0){
        echo "<script> alert('Thông tin này đã tồn tại')</script>";
        redirectURL(getAdminUrl('contact','add'));
    }
    $sql="INSERT INTO contacts (name,email,phone_number,content_text) VALUES ('$name','$email','$phone_number','$content_text');";
    $connect=connect_db();
    mysqli_query($connect,$sql);
    echo "<script> alert('Thêm thông tin thành công')</script>";
    redirectURL(getAdminUrl('contact','list'));
}
?>

