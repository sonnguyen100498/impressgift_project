<?php
$contactID=(!empty($_GET['param']))?$_GET['param']:null;
if(empty($contactID)){
    redirectURL(getAdminUrl('contact','list'));
}
$sql="SELECT * FROM contacts WHERE id=$contactID";
$connect=connect_db();
$contact_query=mysqli_query($connect,$sql);
close_db_connect($connect);
if($contact_query->num_rows==0){
    redirectURL(getAdminUrl('contact','list'));
}
$contact=mysqli_fetch_array($contact_query);
?>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-8">
                <h3 class="card-title mb-0">
                    Contact Management |
                    <small >Edit information</small>
                </h3>
            </div>
        </div>
        <form action="" method="post">
            <div class="form-group mt-4">
                <label for="name">Name</label>
                <input type="text" value="<?php echo $contact['name']?>" id="name" name="name" class="form-control" required>
            </div>
            <div class="form-group mt-4">
                <label for="email">Email</label>
                <input type="email" value="<?php echo $contact['email']?>" id="email" name="email" class="form-control" required>
            </div>
            <div class="form-group mt-4">
                <label for="phone_number">Phone Number</label>
                <input type="tel" value="<?php echo $contact['phone_number']?>" id="phone_number" name="phone_number" class="form-control" required>
            </div>
            <div class="form-group mt-4">
                <label for="content_text">Content Text</label>
                <input type="text" value="<?php echo $contact['content_text']?>" id="content_text" name="content_text" class="form-control" required>
            </div>
            <hr>
            <div class="row">
                <div class="col-6">
                    <button type="submit" name="update" class="btn btn-primary">Update</button>
                </div>
                <div class="col-6 text-right">
                    <a class="btn btn-success" href="<?php echo getAdminUrl('contact','list')?>">Cancel</a>
                </div>
            </div>

        </form>
    </div>
</div>

<?php

if(isset($_POST['update'])){
    $name=(!empty($_POST['name']))?$_POST['name']:null;
    $email=(!empty($_POST['email']))?$_POST['email']:null;
    $phone_number=(!empty($_POST['phone_number']))?$_POST['phone_number']:null;
    $content_text=(!empty($_POST['content_text']))?$_POST['content_text']:null;
    echo $name;
    $sql="UPDATE contacts SET name='$name',email='$email', phone_number='$phone_number',content_text='$content_text' WHERE id=$contactID;";
    $connect=connect_db();
    mysqli_query($connect,$sql);
    echo "<script> alert('Update thông tin thành công')</script>";
    redirectURL(getAdminUrl('contact','list'));

}
?>

