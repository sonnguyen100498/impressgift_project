<?php
$sql = "select id, name from categories order by id desc";
$connect = connect_db();
$categories = mysqli_query($connect, $sql);
close_db_connect($connect);
?>

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-8">
                <h3 class="card-title mb-0">
                    Product Management |
                    <small>Add new product</small>
                </h3>
            </div>
        </div>
        <form action="" method="post" enctype="multipart/form-data">
            <div class="form-group mt-4">
                <select name="category_id" id="category_id" required class="form-control">
                    <option value="">---Chon category---</option>
                    <?php
                        if ($categories->num_rows>0) {
                            while ($category = mysqli_fetch_array($categories)){
                                echo '<option value="'.$category['id'].'">'.$category['name'].'</option>';
                            }
                        }
                    ?>
                </select>
            </div>
            <div class="form-group mt-4">
                <label for="name">Name</label>
                <input type="text" id="name" name="name" class="form-control" required>
            </div>
            <div class="form-group mt-4">
                <label for="price">Price</label>
                <input type="number" id="price" name="price" class="form-control" required>
            </div>
            <div class="form-group mt-4">
                <label for="feature_product">Feature Product</label>
                <select name="feature_product" id="feature_product" required class="form-control">
                    <option value="">-----Chon-----</option>
                    <option value="1">Feature Product</option>
                    <option value="0">not is Feature Product</option>
                </select>
            </div>
            <div class="form-group mt-4">
                <label for="short_description">Short Description</label>
                <textarea name="short_description" id="short_description" rows="5" class="form-control"></textarea>
            </div>
            <div class="form-group mt-4">
                <label for="description">Description</label>
                <textarea name="description" id="description" rows="5" class="form-control"></textarea>
            </div>
            <div class="form-group mt-4">
                <label for="images">Images</label><br>
                <input type="file" id="images" name="images[]"  multiple required>
            </div>
            <hr>
            <div class="row">
                <div class="col-6">
                    <button type="submit" name="submit" class="btn btn-primary">Create</button>
                </div>
                <div class="col-6 text-right">
                    <a class="btn btn-success" href="<?php echo getAdminUrl('product','list')?>">Cancel</a>
                </div>
            </div>

        </form>
    </div>
</div>

<?php
    if (isset($_POST['submit'])) {
        $categoryID = !empty($_POST['category_id'])?$_POST['category_id']: null;
        $name=!empty($_POST['name'])?$_POST['name']:null;
        $price=!empty($_POST['price'])?$_POST['price']:null;
        $feature_product=!empty($_POST['feature_product'])?$_POST['feature_product']:null;
        $shortDescription=!empty($_POST['short_description'])?$_POST['short_description']:null;
        $description=!empty($_POST['description'])?$_POST['description']:null;

        $connect = connect_db();
        $sql = "select * from products where name = '$name'";
        $product = mysqli_query($connect);
        if ($product->num_rows>0) {
            echo "<script> alert('Product đã tồn tại!')</script>";
            redirectURL(getAdminUrl('product','addnew'));
        }

        $total = count($_FILES['images']['name']);
        $image=[];
        for( $i=0 ; $i < $total ; $i++ ) {

            //Get the temp file path
            $tmpFilePath = $_FILES['images']['tmp_name'][$i];

            //Make sure we have a file path
            if ($tmpFilePath != ""){
                //Setup our new file path
                $newFilePath = '../public/imgs/'.$_FILES['images']['name'][$i];
                $images[]=BASE_URL.'/public/imgs/'.$_FILES['images']['name'][$i];
                //Upload the file into the temp dir
                move_uploaded_file($tmpFilePath, $newFilePath);
            }
        }
        $userID=$_SESSION['user_login_id'];
        $sql="INSERT INTO products (category_id,user_id,name,price,is_feature,short_description,description) VALUES ('$categoryID','$userID','$name','$price','$feature_product','$shortDescription','$description');";
        $connect=connect_db();
        mysqli_query($connect,$sql);
        $productID= $connect->insert_id;
        $sql="INSERT INTO product_images (product_id,image) VALUES ";
        foreach ($images as $key=>$image){
            if($key==0){
                $sql.="('$productID','$image')";
            }else{
                $sql.=",('$productID','$image')";
            }
        }
        mysqli_query($connect,$sql);
        close_db_connect($connect);
        echo "<script> alert('Thêm product thành công')</script>";
        redirectURL(getAdminUrl('product','list'));

    }
?>
