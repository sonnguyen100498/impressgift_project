<?php

$id = (!empty($_GET['param'])) ? $_GET['param'] : null;
if (empty($id)) {
    redirectURL(getAdminUrl('users', 'list'));
}

$connect = connect_db();
$sql = "select * from users where id = '$id'";
$user = mysqli_query($connect, $sql);
if ($user -> num_rows == 0){
    redirectURL(getAdminUrl('users', 'list'));
}
$user = mysqli_fetch_array($user);

?>

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-8">
                <h3 class="card-title mb-0">
                    User Management |
                    <small>Edit user</small>
                </h3>
            </div>
        </div>
        <form action="" method="post" enctype="multipart/form-data">
            <div class="form-group mt-4">
                <label for="name">Name</label>
                <input type="text" value="<?php echo $user['name']?>" id="name" name="name" class="form-control" required>
            </div>
            <div class="form-group mt-4">
                <label for="email">Email</label>
                <input type="text" value="<?php echo $user['email']?>" id="email" name="email" class="form-control" required>
            </div>
            <div class="form-group mt-4">
                <label for="password">Password</label>
                <input type="password" id="password" name="password" class="form-control" >
            </div>
            <div class="form-group mt-4">
                <label for="password_confirmmation">Password confirmation</label>
                <input type="password" id="password_confirmmation" name="password_confirmmation" class="form-control">
            </div>
            <div class="form-group mt-4">
                <label for="phone_number">Phone Number</label>
                <input type="tel" value="<?php echo $user['phone_number']?>" id="phone_number" name="phone_number" class="form-control" required>
            </div>
            <div class="form-group mt-4">
                <label for="address">Address</label>
                <input type="text" value="<?php echo $user['address']?>" id="address" name="address" class="form-control" required>
            </div>
            <div class="form-group" mt-4>
                <label for="role">Role</label>
                <select name="role" required id="role" class="form-control">
                    <option value="">--- Choose a role ---</option>
                    <option value="1" <?php echo ($user['role']==1)? 'selected':null ?>>ADMIN</option>
                    <option value="2" <?php echo ($user['role']==2)? 'selected':null ?>>MANAGER</option>
                </select>
            </div>
            <div class="form-group mt-4">
                <label for="avatar">Avatar</label>
                <input type="file" id="avatar" name="avatar" class="form-control">
            </div>
            <div class="mt-3"><?php echo getAvatar($user['avatar'])?></div>
            <div class="row mt-4">
                <div class="col-6">
                    <button type="submit" name="update" class="btn btn-primary">Update</button>
                </div>
                <div class="col-6 text-right">
                    <a class="btn btn-success" href="<?php echo getAdminUrl('users','list')?>">Cancel</a>
                </div>
            </div>
        </form>
    </div>
</div>

<?php
if (isset($_POST['update'])) {
    $name = (!empty($_POST['name']))?$_POST['name']:null;
    $password = (!empty($_POST['password']))?$_POST['password']:null;
    $password_confirmmation = (!empty($_POST['password_confirmmation']))?$_POST['password_confirmmation']:null;
    $email = (!empty($_POST['email']))?$_POST['email']:null;
    $address = (!empty($_POST['address']))?$_POST['address']:null;
    $phone_number = (!empty($_POST['phone_number']))?$_POST['phone_number']:null;
    $role = (!empty($_POST['role']))?$_POST['role']:null;
    if (!empty($password)) {
        if ($password != $password_confirmmation) {
            echo "<script> alert('Xác thực password không chính xác!')</script>";
            redirectURL(getAdminUrl('users','edit',$id));
        }
        $password = md5($password);
    }

    $connect = connect_db();
    $sql = "select * from users email = '$email' and id != '$id'";
    $user = mysqli_query($connect, $sql);
    close_db_connect($connect);
    if ($user -> num_rows > 0) {
        echo "<script> alert('Email đã tồn tại!')</script>";
        redirectURL(getAdminUrl('users','edit',$id));
    }
    if (!in_array($role, ["1", "2"])) {
        echo "<script> alert('Role được chọn không chính xác. Vui lòng chọn lại!')</script>";
        redirectURL(getAdminUrl('users','edit',$id));
    }
    if (!empty($_FILES['avatar']['tmp_name'])){
        if(!in_array($_FILES['avatar']['type'],['image/png','image/jpg','image/jpeg'])){
            echo "<script> alert('File được chọn không phải là hình ảnh!')</script>";
            redirectURL(getAdminUrl('users','edit',$id));
        }
        if($_FILES['avatar']['size']>2048000){
            echo "<script> alert('File quá lớn!')</script>";
            redirectURL(getAdminUrl('users','edit',$id));
        }
        move_uploaded_file($_FILES['avatar']['tmp_name'],'../public/imgs/'.$_FILES['avatar']['name']);
        $avatarURL=BASE_URL.'/public/imgs/'.$_FILES['avatar']['name'];
    }

    $sql="UPDATE users set name='$name', email='$email',phone_number='$phone_number',address='$address',role='$role'";
    if(!empty($password)){
        $sql .=" ,password='$password'";
    }
    if(!empty($avatarURL)){
        $sql .=" ,avatar='$avatarURL'";
    }
    $sql .=" WHERE id='$id'";
    $connect=connect_db();
    mysqli_query($connect,$sql);
    echo "<script> alert('Update user thành công')</script>";
    redirectURL(getAdminUrl('users','list'));
}

?>
