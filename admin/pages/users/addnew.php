<div class="card pr-5">
    <div class="card-body">
        <div class="row">
            <div class="col-8">
                <h3 class="card-title mb-0">
                    User Management |
                    <small>Add new user</small>
                </h3>
            </div>
        </div>
    </div>
    <?php
        $connect = connect_db();
        $sql = "select * from users ORDER BY id DESC ";
        $users = mysqli_query($connect, $sql);
        close_db_connect($connect);
    ?>
    <form action="", method="post", enctype="multipart/form-data">
        <div class="form-group mt-4">
            <label for="name">Name</label>
            <input type="text" id="name" name="name" class="form-control" required>
        </div>
        <div class="form-group mt-4">
            <label for="email">Email</label>
            <input type="email" id="email" name="email" class="form-control" required>
        </div>
        <div class="form-group mt-4">
            <label for="password">Password</label>
            <input type="text" id="password" name="password" class="form-control" required>
        </div>
        <div class="form-group mt-4">
            <label for="password_confirmmation">Password confirmmation</label>
            <input type="text" name="password_confirmmation" id="password_confirmmation" class="form-control" required>
        </div>
        <div class="form-group mt-4">
            <label for="phone_number">Phone number</label>
            <input type="text" id="phone_number" class="form-control" name="phone_number" required>
        </div>
        <div class="form-group mt-4">
            <label for="address">Address</label>
            <input type="text" id="address" class="form-control" name="address" required>
        </div>
        <div class="form-group mt-4">
            <label for="role">Role</label>
            <select name="role" required id="role" class="form-control">
                <option value="">--- Choose a role ---</option>
                <option value="1">ADMIN</option>
                <option value="2">MANAGER</option>
            </select>
        </div>
        <div class="form-group mt-4">
            <label for="avatar">Avatar</label>
            <input type="file" id="avatar" name="avatar" class="form-control" required>
        </div>
        <hr>
        <div class="row">
            <div class="col-6">
                <button type="submit" name="submit" class="btn btn-primary">Add new user</button>
            </div>
            <div class="col-6 text-right">
                <a class="btn btn-success" href="<?php echo getAdminUrl('users','list')?>">Cancel</a>
            </div>
        </div>

    </form>
</div>

<?php
if(isset($_POST['submit'])) {
    $name = (!empty($_POST['name']))?$_POST['name']:null;
    $password = (!empty($_POST['password']))?$_POST['password']:null;
    $password_confirmmation = (!empty($_POST['password_confirmmation']))?$_POST['password_confirmmation']:null;
    $email = (!empty($_POST['email']))?$_POST['email']:null;
    $address = (!empty($_POST['address']))?$_POST['address']:null;
    $phone_number = (!empty($_POST['phone_number']))?$_POST['phone_number']:null;
    $role = (!empty($_POST['role']))?$_POST['role']:null;
    if ($password != $password_confirmmation) {
        echo "<script> alert('Xac thuc password khong chinh xac') </script>";
        redirectURL(getAdminUrl('users', 'addnew'));
    }
    $password = md5($password);
    $connect = connect_db();
    $sql = "select * from users where email = '$email'";
    $user = mysqli_query($connect, $sql);
    close_db_connect($connect);
    if ($user->num_rows > 0) {
        echo "<script>alert('Email da ton tai')</script>";
        redirectURL(getAdminUrl('users', 'addnew'));
    }
    if (!in_array($role, ["1", "2"])) {
        echo "<script>alert('Role duoc chon khong chinh xac')</script>";
        redirectURL(getAdminUrl('users', 'addnew'));
    }
    if (!in_array($_FILES['avatar']['type'], ['image/png','image/jpg','image/jpeg'])){
        echo "<script> alert('File được chọn không phải là hình ảnh!')</script>";
        redirectURL(getAdminUrl('user','addnew'));
    }
    if($_FILES['avatar']['size']>2048000){
        echo "<script> alert('File quá lớn!')</script>";
        redirectURL(getAdminUrl('user','addnew'));
    }
    move_uploaded_file($_FILES['avatar']['tmp_name'], '../public/imgs/'.$_FILES['avatar']['name']);
    $avatarURL=BASE_URL.'/public/imgs/'.$_FILES['avatar']['name'];
    $sql="INSERT INTO users (name,email,password,phone_number,address,role,avatar) VALUES ('$name','$email','$password','$phone_number','$address','$role','$avatarURL');";
    $connect=connect_db();
    mysqli_query($connect,$sql);
    echo "<script> alert('Thêm user thành công')</script>";
//    header("location: ".getAdminUrl('user','list'));
    echo("<script>location.href = '".BASE_URL."/admin/?controller=users&action=list';</script>");
//    BASE_URL."/admin?controller=$controller&action=$action&param=$param"
}


?>