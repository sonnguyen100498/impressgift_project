
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-8">
                <h3 class="card-title mb-0">
                    Category Management |
                    <small >Add new category</small>
                </h3>
            </div>
        </div>
        <form action="" method="post" >
            <div class="form-group mt-4">
                <label for="name">Name</label>
                <input type="text" id="name" name="name" class="form-control" required>
            </div>
            <div class="form-group mt-4">
                <label for="description">Description</label>
                <textarea name="description" id="description" rows="5" class="form-control"></textarea>
            </div>

            <hr>
            <div class="row">
                <div class="col-6">
                    <button type="submit" name="submit" class="btn btn-primary">Create</button>
                </div>
                <div class="col-6 text-right">
                    <a class="btn btn-success" href="<?php echo getAdminUrl('category','list')?>">Cancel</a>
                </div>
            </div>

        </form>
    </div>
</div>

<?php
if (isset($_POST['submit'])) {
    $name = (!empty($_POST['name'])) ? $_POST['name'] : null;
    $description = (!empty($_POST['description'])) ? $_POST['description'] : null;
    $connect = connect_db();
    $sql = "select * from categories where name ='$name'";
    $category = mysqli_query($connect, $sql);
    close_db_connect($connect);
    if ($category->num_rows>0) {
        echo "<script> alert('Category đã tồn tại!')</script>";
        redirectURL(getAdminUrl('category','addnew'));
    }
    $sql="INSERT INTO categories (name,description) VALUES ('$name','$description');";
    $connect=connect_db();
    mysqli_query($connect,$sql);
    echo "<script> alert('Thêm category thành công')</script>";
    redirectURL(getAdminUrl('category','list'));
}

?>
