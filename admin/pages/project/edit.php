<?php
$projectId = (!empty($_GET['param']))?$_GET['param']:null;
if (empty($projectId)) {
    redirectURL(getAdminUrl('project', 'list'));
}
$sql = "select * from projects where id = '$projectId'";
$connect = connect_db();
$project_query = mysqli_query($connect, $sql);
close_db_connect($connect);
if ($project_query->num_rows == 0) {
    redirectURL(getAdminUrl('project', 'list'));
}
$project = mysqli_fetch_array($project_query);
?>

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-8">
                <h3 class="card-title mb-0">
                    Project Management |
                    <small>Edit project</small>
                </h3>
            </div>
        </div>
        <form action="" method="post" enctype="multipart/form-data">
            <div class="form-group mt-4">
                <label for="name">Name</label>
                <input type="text" value="<?php echo $project['name']?>" id="name" name="name" class="form-control" required>
            </div>

            <div class="form-group mt-4">
                <label for="image">Image</label>
                <input type="file" id="image" name="image" class="form-control" >
            </div>
            <div class="mt-4">
                <?php echo getAvatar($project['image'])?>
            </div>
            <hr>
            <div class="row mt-4">
                <div class="col-6">
                    <button type="submit" name="update" class="btn btn-primary">Update</button>
                </div>
                <div class="col-6 text-right">
                    <a class="btn btn-success" href="<?php echo getAdminUrl('project','list')?>">Cancel</a>
                </div>
            </div>
        </form>
    </div>
</div>

<?php
if (isset($_POST['update'])) {
    $name = (!empty($_POST['name']))?$_POST['name']:null;
    $connect = connect_db();
    $sql = "select * from projects where name = '$name' and id != '$projectId'";
    $project = mysqli_query($connect, $sql);
    close_db_connect($connect);
    if ($project->num_rows>0) {
        echo "<script> alert('Tên project đã tồn tại!')</script>";
        redirectURL(getAdminUrl('project','edit', ));
    }
    if(!empty($_FILES['image']['tmp_name'])){
        if(!in_array($_FILES['image']['type'],['image/png','image/jpg','image/jpeg'])){
            echo "<script> alert('File được chọn không phải là hình ảnh!')</script>";
            redirectURL(getAdminUrl('project','edit', $projectId));
        }
        if($_FILES['image']['size']>2048000){
            echo "<script> alert('File quá lớn!')</script>";
            redirectURL(getAdminUrl('project','edit',$projectId));
        }
        move_uploaded_file($_FILES['image']['tmp_name'],'../public/imgs/'.$_FILES['image']['name']);
        $imageURL=BASE_URL.'/public/imgs/'.$_FILES['image']['name'];
    }
    $sql = "UPDATE projects set name ='$name'";
    if (!empty($imageURL)) {
        $sql .= " ,image='$imageURL'";
    }
    $sql .= " WHERE id = '$projectId'";
    $connect = connect_db();
    mysqli_query($connect, $sql);
    echo "<script> alert('Update project thành công')</script>";
    redirectURL(getAdminUrl('project','list'));
}
?>

