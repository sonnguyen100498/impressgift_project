<?php
$connect = connect_db();
$sql = "select * from projects order by id desc ";
$projectQuery = mysqli_query($connect, $sql);
if ($projectQuery -> num_rows>0) {
    $projects = mysqli_fetch_array($projectQuery);
}
?>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title mb-0">
                    Projects Management |
                    <small>List Projects</small>
                </h4>
            </div>
            <div class="col-4 text-right">
                <a href="<?php echo getAdminUrl('project', 'addnew')?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>Add New Project</a>
            </div>
        </div>
        <div class="mt-4">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <td><strong>ID</strong></td>
                        <td><strong>Name</strong></td>
                        <td><strong>Image</strong></td>
                        <td><strong>Action</strong></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($projectQuery->num_rows>0){
                        while ($project=mysqli_fetch_array($projectQuery)){
                            ?>
                            <tr>
                                <td><?php echo $project['id']?></td>
                                <td><?php echo $project['name']?></td>
                                <td><?php echo getAvatar($project['image'])?></td>
                                <td><a href="<?php echo getAdminUrl('project','edit',$project['id']) ?>" class="btn btn-warning">Edit</a></td>
                                <td><a href="<?php echo getAdminUrl('project','delete',$project['id']) ?>" class="btn btn-danger">Delete</a></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
