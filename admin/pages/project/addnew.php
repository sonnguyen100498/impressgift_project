<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-8">
                <h3 class="card-title mb-0">
                    Projects Management |
                    <small> Add new project</small>
                </h3>
            </div>
        </div>
        <form action="" method="post" enctype="multipart/form-data">
            <div class="form-group mt-4">
                <label for="name">Name</label>
                <input type="text" id="name" name="name" class="form-control" required>
            </div>
            <div class="form-group mt-4">
                <label for="avatar">Image</label>
                <input type="file" id="image" name="image" class="form-control" required>
            </div>
            <hr>
            <div class="row">
                <div class="col-6">
                    <button type="submit" name="submit" class="btn btn-primary">Add new project</button>
                </div>
                <div class="col-6 text-right">
                    <a class="btn btn-success" href="<?php echo getAdminUrl('project','list')?>">Cancel</a>
                </div>
            </div>
        </form>
    </div>
</div>

<?php
    if (isset($_POST['submit'])) {
        $name = (!empty($_POST['name']))?$_POST['name']:null;
        $connect = connect_db();
        $sql = "select * from projects where name '$name'";
        $project = mysqli_query($connect, $sql);
        if ($project->num_rows>0) {
            echo "<script> alert('Tên project đã tồn tại!')</script>";
            redirectURL(getAdminUrl('project','addnew'));
        }
        if(!in_array($_FILES['image']['type'],['image/png','image/jpg','image/jpeg'])){
            echo "<script> alert('File được chọn không phải là hình ảnh!')</script>";
            redirectURL(getAdminUrl('project','addnew'));
        }
        if($_FILES['image']['size']>2048000){
            echo "<script> alert('File quá lớn!')</script>";
            redirectURL(getAdminUrl('project','addnew'));
        }
        move_uploaded_file($_FILES['image']['tmp_name'],'../public/imgs/'.$_FILES['image']['name']);
        $imageURL=BASE_URL.'/public/imgs/'.$_FILES['image']['name'];
        $sql="INSERT INTO projects (name,image) VALUES ('$name','$imageURL');";
        $connect=connect_db();
        mysqli_query($connect,$sql);
        echo "<script> alert('Thêm project thành công')</script>";
        redirectURL(getAdminUrl('project','list'));
    }
?>
