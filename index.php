<?php
session_start();
include "config/connection.php";

include "pages/include/header.php";

if(!empty($_GET['action'])){
    if(file_exists("pages/include/".$_GET['action'].".php")) {
        include "pages/include/". $_GET['action'] . ".php";
    }
} else {
    include "pages/include/home.php";
}
include "pages/include/footer.php"
?>